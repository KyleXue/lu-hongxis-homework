import re
import tkinter as tk
from tkinter import messagebox

EXCHANGE_RATE = 7.26

def convert_currency():
    input_str = entry.get().strip().lower()
    
    if input_str == "off":
        messagebox.showinfo("退出", "程序已退出")
        root.destroy()
        return
    
    # 使用正则表达式匹配数字和货币单位
    match = re.match(r"^(\d+\.?\d*)\s*([a-z]+)$", input_str)
    if not match:
        messagebox.showerror("输入错误", "输入格式错误，请重新输入（示例：100usd 或 200CNY）")
        return
    
    amount, currency = match.groups()
    amount = float(amount)
    
    if currency == "usd":
        result = amount * EXCHANGE_RATE
        messagebox.showinfo("换算结果", f"{amount:.2f} USD = {result:.2f} CNY")
    elif currency == "cny":
        result = amount / EXCHANGE_RATE
        messagebox.showinfo("换算结果", f"{amount:.2f} CNY = {result:.2f} USD")
    else:
        messagebox.showerror("货币错误", f"不支持的货币类型：{currency.upper()}，请输入USD或CNY")

# 创建主窗口
root = tk.Tk()
root.title("汇率换算程序")
root.geometry("400x200")

# 添加标签
label = tk.Label(root, text="请输入金额和货币（如100USD或500CNY）：")
label.pack(pady=10)

# 添加输入框
entry = tk.Entry(root, width=30)
entry.pack(pady=10)

# 添加按钮
convert_button = tk.Button(root, text="换算", command=convert_currency)
convert_button.pack(pady=10)

# 添加退出按钮
exit_button = tk.Button(root, text="退出", command=root.destroy)
exit_button.pack(pady=10)

# 运行主循环
root.mainloop()
