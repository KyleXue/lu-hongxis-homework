import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report, accuracy_score
from sklearn.ensemble import GradientBoostingClassifier

# 加载数据
train_data = pd.read_csv("train_dataset_with_outliers.csv")
test_data = pd.read_csv("test_dataset_with_outliers.csv")

# 特征和目标变量分离
X = train_data.drop(columns=["Exited", "RowNumber", "CustomerId", "Surname"])
y = train_data["Exited"]

# 数值和分类特征
numerical_columns = ["CreditScore", "Age", "Tenure", "Balance", "NumOfProducts", "EstimatedSalary"]
categorical_columns = ["Geography", "Gender", "HasCrCard", "IsActiveMember",
                       "is_CreditScore_outlier", "is_Age_outlier", "is_NumOfProducts_outlier"]

# 数据预处理
preprocessor = ColumnTransformer(
    transformers=[
        ("num", StandardScaler(), numerical_columns),
        ("cat", OneHotEncoder(drop="first"), categorical_columns),
    ]
)

# 定义GBDT模型
gbdt_model = Pipeline(steps=[
    ("preprocessor", preprocessor),
    ("classifier", GradientBoostingClassifier(random_state=42, subsample=0.8, max_depth=3, learning_rate=0.1))
])

# 划分训练集和验证集
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)

# 训练模型
gbdt_model.fit(X_train, y_train)

# 验证集预测
y_pred_val = gbdt_model.predict(X_val)

# 评估模型
accuracy_val = accuracy_score(y_val, y_pred_val)
report_val = classification_report(y_val, y_pred_val)

print("Validation Accuracy:", accuracy_val)
print("Validation Report:\n", report_val)

# 测试集特征与目标变量分离
X_test = test_data.drop(columns=["Exited", "RowNumber", "CustomerId", "Surname"])
y_test = test_data["Exited"]

# 测试集预测
y_pred_test = gbdt_model.predict(X_test)

# 评估测试集结果
accuracy_test = accuracy_score(y_test, y_pred_test)
report_test = classification_report(y_test, y_pred_test)

print("Test Accuracy:", accuracy_test)
print("Test Report:\n", report_test)

# 保存测试集预测结果
test_data["Predicted_Exited"] = y_pred_test
test_data.to_csv("test_dataset_with_predictions.csv", index=False)

# 提取特征重要性
feature_importances = gbdt_model.named_steps["classifier"].feature_importances_
feature_names = numerical_columns + list(preprocessor.named_transformers_['cat'].get_feature_names_out())

# 绘制特征重要性图
sorted_indices = np.argsort(feature_importances)[::-1]
plt.figure(figsize=(10, 6))
plt.barh(np.array(feature_names)[sorted_indices][:10], feature_importances[sorted_indices][:10], color="steelblue")
plt.xlabel("Importance")
plt.ylabel("Features")
plt.title("Top 10 Feature Importances")
plt.gca().invert_yaxis()
plt.show()

import shap

# 对训练数据进行手动预处理
X_train_preprocessed = gbdt_model.named_steps["preprocessor"].transform(X_train)

# 确保数据类型为 NumPy 数组和 float64
X_train_preprocessed = X_train_preprocessed.toarray() if hasattr(X_train_preprocessed, 'toarray') else X_train_preprocessed
X_train_preprocessed = X_train_preprocessed.astype('float64')

# 提取数值特征和分类特征名
numerical_features = numerical_columns
categorical_features = preprocessor.named_transformers_['cat'].get_feature_names_out(categorical_columns)
all_features = list(numerical_features) + list(categorical_features)

# 创建 SHAP Explainer
explainer = shap.Explainer(gbdt_model.named_steps["classifier"], X_train_preprocessed)

# 计算 SHAP 值
shap_values = explainer(X_train_preprocessed)

# 绘制 SHAP 特征重要性图（条形图）
shap.summary_plot(shap_values, X_train_preprocessed, feature_names=all_features, plot_type="bar")

# 绘制 SHAP 特征重要性图（详细图）
shap.summary_plot(shap_values, X_train_preprocessed, feature_names=all_features)
