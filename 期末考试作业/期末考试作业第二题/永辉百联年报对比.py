import pandas as pd

# 读取数据文件
file_path = '永辉百联对比数据.csv'  # 替换为实际文件路径
data = pd.read_csv(file_path)

# 计算净资产收益率ROE (净利润 / (总资产 - 总负债))
data['ROE_永辉'] = (data['净利润(亿元)_永辉'] / (data['总资产(亿元)_永辉'] - data['总负债(亿元)_永辉'])) * 100
data['ROE_百联'] = (data['净利润(亿元)_百联'] / (data['总资产(亿元)_百联'] - data['总负债(亿元)_百联'])) * 100

# 将ROE保留两位小数并加上%
data['ROE_永辉'] = data['ROE_永辉'].apply(lambda x: f"{x:.2f}%")
data['ROE_百联'] = data['ROE_百联'].apply(lambda x: f"{x:.2f}%")

# 打印结果
print(data[['年份', 'ROE_永辉', 'ROE_百联']])

# 保存结果到文件
data.to_csv('永辉百联_ROE计算结果_优化版.csv', index=False)

import matplotlib.pyplot as plt

# 确保数据格式正确，去掉百分号并转换为浮点数
if data['ROE_永辉'].dtype == 'object':
    data['ROE_永辉'] = data['ROE_永辉'].str.rstrip('%').astype(float)
if data['ROE_百联'].dtype == 'object':
    data['ROE_百联'] = data['ROE_百联'].str.rstrip('%').astype(float)

# 提取需要可视化的数据
years = data['年份']
roe_yh = data['ROE_永辉']
roe_bl = data['ROE_百联']

# 绘制对比柱状图和折线图
plt.figure(figsize=(10, 6))

# 绘制柱状图
width = 0.35  # 设置柱宽
x = range(len(years))
plt.bar([i - width/2 for i in x], roe_yh, width=width, label="Yonghui ROE", color='skyblue')
plt.bar([i + width/2 for i in x], roe_bl, width=width, label="Bailian ROE", color='orange')

# 绘制折线图
plt.plot(x, roe_yh, marker='o', color='blue', label="Yonghui ROE Trend", linestyle='--')
plt.plot(x, roe_bl, marker='s', color='darkorange', label="Bailian ROE Trend", linestyle='-')

# 添加图表元素
plt.title("ROE Comparison: Yonghui vs Bailian (2021-2023)", fontsize=14)
plt.xlabel("Year", fontsize=12)
plt.ylabel("ROE (%)", fontsize=12)
plt.xticks(ticks=x, labels=years, fontsize=10)  # 横轴显示年份
plt.axhline(0, color='red', linestyle='--', linewidth=0.8, label='Break-even Line')
plt.legend(fontsize=12)
plt.grid(True, linestyle='--', alpha=0.6)

# 显示图表
plt.tight_layout()
plt.show()
